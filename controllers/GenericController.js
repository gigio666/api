module.exports = function (routeName) {
    
//     var fs = require('fs');
//     var saveObject = function (event) {
//         var dir = './results';
//         var routeDir = dir + "/" + routeName
// 
//         if (!fs.existsSync(dir)) {
//             fs.mkdirSync(dir);
//         }
// 
//         if (!fs.existsSync(routeDir)) {
//             fs.mkdirSync(routeDir);
//         }
// 
//         var fileDir = routeDir + "/" + event.object.number + ".js";
//         fs.writeFile(fileDir, JSON.stringify(event.object), function (err) {
//             if (err) {
//                 return console.log(err);
//             }
//             console.log("New event saved! data:")
//             console.log(event);
//         });
//     }

    var doRouteOk = function (req, res) {
        console.log("route: " + routeName);
        res.status(200);
        //saveObject(req.body);
        console.log(req.body);
        res.json({
            status: 'ok',
            message: 'event received',
            event: req.body
        });
    }

    var doRouteNoResponse = function (req, res) {
    }

    var getIndex = function (req, res) {
        res.send('this is not a valid route');
    }

    var doBadRequest = function (req, res) {
        res.status(400);
        res.send('bad request');
    }

    var doUnauthorized = function (req, res) {
        res.status(401);
        res.send('unauthorized');
    }


    var doNotFound = function (req, res) {
        res.status(404);
        res.send('not found');
    }

    var doInternalError = function (req, res) {
        res.status(500);
        res.send('internal error');
    }


    return {
        DoRouteOk: doRouteOk,
        GetIndex: getIndex,
        DoRouteNoResponse: doRouteNoResponse,
        DoBadRequest: doBadRequest,
        DoUnauthorized: doUnauthorized,
        DoNotFound: doNotFound,
        DoInternalError: doInternalError
    }
}