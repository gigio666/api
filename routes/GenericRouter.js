module.exports = function (routeName) {

    var express = require('express');
    var xrouter = express.Router();
    var controller = require('../controllers/GenericController')(routeName);

    xrouter.route('/ok')
        .post(controller.DoRouteOk);

    xrouter.route('/noresponse')
        .post(controller.DoRouteNoResponse);

    xrouter.route('/badrequest')
        .post(controller.DoBadRequest);

    xrouter.route('/internalerror')
        .post(controller.DoInternalError);

    xrouter.route('/notfound')
        .post(controller.DoNotFound);

    xrouter.route('/')
        .get(controller.GetIndex);

    return xrouter;

}