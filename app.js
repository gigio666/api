var express = require('express');
var bodyParser = require('body-parser');

// var fs = require('fs');
// var path = "./results";
// var deleteFolderRecursive = function(path) {
//   if( fs.existsSync(path) ) {
//     fs.readdirSync(path).forEach(function(file,index){
//       var curPath = path + "/" + file;
//       if(fs.lstatSync(curPath).isDirectory()) { // recurse
//         deleteFolderRecursive(curPath);
//       } else { // delete file
//         fs.unlinkSync(curPath);
//       }
//     });
//     fs.rmdirSync(path);
//   }
// };
// 
// deleteFolderRecursive(path);

var app = express();
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: true
}));

app.use('/Salesforce', require('./routes/GenericRouter')('salesforce'));
app.use('/Tempo', require('./routes/GenericRouter')('tempo'));
app.use('/Mundipagg', require('./routes/GenericRouter')('mundipagg'));
app.use('/Eventtracker', require('./routes/GenericRouter')('eventtracker'));

var port = 1338;

app.listen(port, function (err) {
    console.log('Running publisher api from port ' + port);
    if (err) {
        console.log('error: ' + err);
    }
});